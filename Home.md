# **Вики REST API 2bedone!**

## *Работа с задачами:*

* ### [Добавление](https://bitbucket.org/todoshka/2bedone-doc/wiki/add%20task).

## *Работа с идеями:*

* ### [Добавление](https://bitbucket.org/todoshka/2bedone-doc/wiki/add%20idea).

## *Работа со встречами:*

* ### [Добавление](https://bitbucket.org/todoshka/2bedone-doc/wiki/add%20meet).